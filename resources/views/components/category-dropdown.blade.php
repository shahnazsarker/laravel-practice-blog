<x-dropdown>
    <x-slot name="trigger">
        <button class="py-2 text-left px-3 pr-5 lg:w-32 w-full text-sm font-semibold flex lg:inline-flex" @click="show = ! show">
            {{ isset($currentCategory)? ucwords($currentCategory->name) : 'Categories' }}
            <x-icon name="down-arrow" class="absolute pointer-events-none" style="right: 12px;"/>
        </button>
    </x-slot>
    {{--             <a href='/' class="block  pl-3 rounded-md hover:bg-blue-300 focus:bg-blue-400 hover:text-white focus:text-white"--}}
    {{--                > All</a>--}}
    <x-dropdown-item href="/" :active="request()->routeIs('home')"> All </x-dropdown-item>
    @foreach( $categories as $category)
        <x-dropdown-item href="/?category={{ $category->slug }}"
                         :active='request()->is("$categories/.{$category->slug}")'
        >{{ ucwords($category->name) }}</x-dropdown-item>

    @endforeach
</x-dropdown>
