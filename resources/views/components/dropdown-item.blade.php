@props(['active'=>false])
@php
$classes='block py-1  px-3  my-1 leading-6 rounded-md hover:bg-blue-400 focus:bg-blue-400 hover:text-white focus:text-white';
if ($active) $classes= 'block bg-blue-400 text-white px-3 leading-6 mx-1 py-1 rounded-md';
@endphp

<a {{ $attributes(['class'=> $classes]) }}>
    {{ $slot }}</a>
