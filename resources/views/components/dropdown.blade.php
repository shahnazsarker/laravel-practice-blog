@props(['trigger'])
<div  x-data="{ show: false }" @click.away="show = false">
    {{--    Trigger  --}}
    <div @click="show =! show">
        {{ $trigger }}

    </div>
    <div class=" text-left mt-2 absolute lg:w-32 w-full bg-gray-100 rounded-xl z-50 overflow-auto max-h-48" x-show="show" style="display: none">
        {{--Links--}}
       {{$slot}}
    </div>
</div>
