### Laravel Basic Blog
Laravel is a web application framework with expressive, elegant syntax.
## How Web Request Works in Laravel
___
* Laravel takes a request and goes to routes file.
* web.php or api.php or custom routes file.
* looks for the given route and loads the controller.
* The controller receives the request and provides the right view. Controller talks to database with Eloquent ORM.
### Prerequisites
* Server ( Apache or nginx)
* Database Server (MySQL, SQLite, MariaDB)
* PHP Composer package manager
## Laravel Installation
___
#### Via Laravel Installer
**First, download the Laravel installer using Composer.** 
>composer global require "laravel/installer=~1.1"\
> laravel new example-app\
> cd example-app \
> php artisan serve

**clone Laravel project from git**
>composer install\
cp .env.example .env\
php artisan key:generate
## Route
___
* Folder Name: routes
* Basic Route

> use Illuminate\Support\Facades\Route;
> 
>Route::get('/', function(){\
return view('welcome');\
});
* Add path
> Route::get('posts/{post}', funtion($slug){\
$path = __DIR__ . "/../resourcess/posts/{$slug}.html";\
if(!file_exists($path)){\
return redirect('/');\
}\
$post = file_get_contents($path);\
return view('post', ['post' => $post]);\
});
* URL Constrains
> Route::get('posts/{post}', funtion($slug){\
$path = __DIR__ . "/../resourcess/posts/{$slug}.html";\
if(!file_exists($path)){\
return redirect('/');\
}\
$post = file_get_contents($path);\
return view('post', ['post' => $post]);\
})->where('post', "[A-z_\-]+");
* Cache
>Route::get('posts/{post}', funtion($slug){\
$path = __DIR__ . "/../resourcess/posts/{$slug}.html";\
if(!file_exists($path)){\
return redirect('/');\
}\
// Key, time, closure\
$post = cache()->remember(\
"posts.{$slug}", 1200, fn() => file_get_contents($path));\
return view('post', ['post' => $post]);\
})->where('post', "[A-z_\-]+");
## Views
___
* Folder Name: resources/view
* Add Custom CSS and JavaScript to public folder.

* {{ }} this renders the variable with safe format with
htmlspecialchars() function.

* {!! !!} this renders the variable without the safe format
## Migrate
___
* up –> $table->create(‘column Name’);
* down –> $table->drop(‘column Name’);
* get help
>php artisan help make:model

## Seven Rest Functions
___

* C : Create

      create: Create a object.
      store: Store the object.


* U: Update

      edit: Edit a Single object.
      update: Update that object.


* R: Read

      index: Show List of objects.
      show: Show Single object.


* D: Delete

      destroy: Delete a object.
    
## Routes for Seven REST functions
___

* index:   GET    objects
* show:    GET    object:id/
* create:  GET    object/create (View HTML Form)
* store:   POST   objects
* edit:    GET    object:id/edit/ (View HTML Form)
* update:  PUT    object:id/
* destroy: DELETE object:id/\
Example-
>Route::get('articles', ['ArticleController', 'index']);\
Route::get('aricles/create', ['ArticleController', 'create']);\
Route::post('articles', ['ArticleController', 'store']);\
Route::get('articles/{id}', ['ArticleController', 'show']);\
Route::get('articles/{id}/edit', ['ArticleController', 'edit']);\
Route::put('articles/{id}', ['ArticleController', 'update']);\
Route::delete('articles/{id}', ['ArticleController', 'destroy']);
## Eloquent Relationship
___
* hasOne
* hasMany
> // User Model\
public function articles(){\
// select * from articles where user_id = $this->id;\
return $this->hasMany(Article::class);\
}
* belongsTo
>// Article Model\
public function user(){\
// select * from user where article_id = $this->id;\
return $this->belongsTo(User::class);\
}
* belongsToMany
>// Article Model\
public function tags(){\
return $this->belongsToMany(Tag::class);\
}\
// Tag Model\
     public function article(){\
	return $this->belongsToMany(Article::class);\
     }
    
## Eloquent Relation Query
___
>// select * from user where id = 1;\
$user = User::find(1);\
// select * from atricles where user_id = 1;\
$user->articles;\
// or find one\
$user->article->find(1);
